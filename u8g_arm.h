/*
 * u8g_arm.h
 *
 *  Created on: 27.12.2016
 *      Author: chris
 */

#ifndef U8G_ARM_H_
#define U8G_ARM_H_

#include "u8g.h"
#include "stm32f0xx_hal.h"

 #define SPI_HANDLER hspi1 // use your SPI hadler
 extern SPI_HandleTypeDef SPI_HANDLER;

 uint8_t u8g_com_hw_spi_fn(u8g_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr);

#endif /* U8G_ARM_H_ */
